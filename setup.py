import re
import sys

from setuptools import find_packages, setup

setup(
    name="matrix-latex-bot",
    version="0.0.1",
    description="Matrix bot to display LaTeX equation",
    long_description=open('README.rst').read(),
    maintainer="Otthorn",
    license='GPLv3',
    keywords=['matrix', 'latex', 'bot'],
    url="https://gitlab.crans.org/Otthorn/matrix_latex_bot",
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    packages=['matrix_latex_bot'],
    include_package_data=True,
    install_requires=[
        'matrix-nio>=0.9',
    ],
    tests_require=[],
)