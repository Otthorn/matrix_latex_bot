import subprocess


def equation(eq):

    tex = r"\documentclass{standalone}"\
        r"\begin{document}"\
        r"$\displaystyle " + eq + r" $"\
        r"\end{document}"

    # print(tex)

    f = open("/tmp/tmp.tex", "w")
    f.write(tex)
    f.close()


def create_png(core):
    equation(core)  # write to /tmp/tmp.tex
    subprocess.run("latex2png -d 600 /tmp/tmp.tex".split())  # create the png
