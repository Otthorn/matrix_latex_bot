class Message(object):

    def __init__(self, client, message_content, room, event):

        self.client = client
        self.message_content = message_content
        self.room = room
        self.event = event

    async def upload_image(self, path, content_type):
        uri = await self.client.upload(
            data_provider=lambda x, y: path,
            content_type=content_type)
        return uri

    async def send_image(self, url):
        print("[DEBUG] Sending image...")
        await self.client.room_send(
            room_id=self.room.room_id,
            message_type="m.room.message",
            content={
                "msgtype": "m.image",
                "body": "latex_bot.png",
                "url": url
            }
        )
        print("[DEBUG] Image sent.")
