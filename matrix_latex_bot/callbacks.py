from .message import Message
from .latex import create_png


class Callbacks(object):

    def __init__(self, client):
        self.client = client

    async def message_cb(self, room, event):

        # Ignore messages from the bot itself
        if event.sender == self.client.user:
            return

        msg = event.body

        # verify the latex format
        if msg[0] == "$" and msg[-1] == "$":
            core = msg[1:-1]
            create_png(core)

            print(
                f"[DEBUG] sender = {event.sender} in {room.display_name} : {core}")

            message = Message(self.client, msg, room, event)

            upload_response = await message.upload_image("/tmp/tmp.png", "image/png")
            uri = upload_response[0].content_uri

            print(f"[DEBUG] uri = {uri}")

            await message.send_image(uri)

        return
