import configparser
import asyncio
import argparse
import logging

import nio

from .callbacks import Callbacks
from .exceptions import MisconfiguredError

# Module logger
log = logging.getLogger('matrix_latex_bot')


def parse_command_line() -> argparse.Namespace:
    """
    Parse command line and check that config file is readable
    """
    parser = argparse.ArgumentParser(
        prog="matrix_latex_bot",
        description="Matrix bot to display LaTeX equation")
    parser.add_argument("config",
                        help="path to the configuration file",
                        type=argparse.FileType('r'))
    parser.add_argument("--debug",
                        help="show debug messages",
                        action="store_true")
    return parser.parse_args()


def parse_config(config_path: str) -> configparser.ConfigParser:
    """
    Check and parse configuration
    """
    log.info(f"Parsing configuration file \"{config_path}\"")
    config = configparser.ConfigParser()
    config.read(config_path)
    if "Matrix" not in config:
        raise MisconfiguredError("Missing [Matrix] group in configuration")
    for c in ['HomeServer', 'Username', 'Password']:
        if c not in config["Matrix"]:
            raise MisconfiguredError(f"Missing {c} in [Matrix]")
    return config


async def main(config_path):
    config = parse_config(config_path)
    homeserver = config["Matrix"]["HomeServer"]
    username = config["Matrix"]["Username"]
    password = config["Matrix"]["Password"]

    # displayname defaults to username if not set
    displayname = config["Matrix"].get("DisplayName", username)

    # Connect to homeserver with credentials
    client = nio.AsyncClient(homeserver, username)
    await client.login(password)

    # Change display name if differ from configuration
    if displayname != client.get_displayname():
        client.set_displayname(displayname)

    # Add callbacks
    callbacks = Callbacks(client)
    client.add_event_callback(callbacks.message_cb, nio.RoomMessageText)

    await client.sync_forever(timeout=30000)


if __name__ == "__main__":
    # Set up global logging with nice colors
    logging.addLevelName(logging.INFO, "\033[1;36mINFO\033[1;0m")
    logging.addLevelName(logging.WARNING, "\033[1;33mWARNING\033[1;0m")
    logging.addLevelName(logging.ERROR, "\033[1;91mERROR\033[1;0m")
    logging.addLevelName(logging.DEBUG, "\033[1;37mDEBUG\033[1;0m")
    logging.basicConfig(
        level=logging.DEBUG,
        format='\033[1;90m%(asctime)s\033[1;0m %(name)s %(levelname)s %(message)s')

    # Parse arguments
    args = parse_command_line()

    # Launch bot
    asyncio.get_event_loop().run_until_complete(main(args.config.name))
