FROM python:3.8-buster

WORKDIR /usr/src/app

COPY . .
RUN pip install --no-cache-dir .

CMD [ "python", "-m", "matrix_latex_bot" ]