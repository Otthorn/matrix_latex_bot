Matrix LaTeX Bot
================

matrix-latex-bot is a bot for the Matrix communcation protocol. It aims
to automatically send images of corresponding LaTeX from given commands.

It is a proof of concept and not fully fonctionnal yet.

Installation
------------

With Python VirtualEnv
~~~~~~~~~~~~~~~~~~~~~~

For developement purposes, you can use a Python 3.6+ VirtualEnv. To
create a new VirtualEnv in this project, run

.. code:: bash

   python3 -m venv venv
   . venv/bin/activate
   pip install -e .

Also install a working TeXlive environment.

Then you will be able to launch the project with
``python -m matrix_latex_bot``.

With Docker
~~~~~~~~~~~

For production you may build a Docker image.

.. code:: bash

   docker build -t matrix-latex-bot .

Usage
-----

In order to use the bot, you need to be in the same room as it and send
a message beggining and ending by a single ``$``. For example:

::

   $y = ax^2 + bx + c$

TODO-list and known bugs
------------------------

-  Auto-join rooms on invitations
-  Stop crashing on malformed latex and print an error (``latex.py``)
-  Only react to new messages  [1]_
-  Add more options to the ``config.ini`` file: latex2png options for
   example.

.. [1]
   Now it reads every messages, including the old ones each time the bot
   restarts. This could be fixed with a timer (only <1min old messages).
